class Storage
{
    getToken()
    {
        const parameters = this.getQueryStringParameters(window.location.hash);
        return parameters['access_token'];
    }

    get(index)
    {
        return localStorage.getItem(index);
    }

    set(index, value)
    {
        localStorage.setItem(index, value);
    }

    /**
     * @see https://stackoverflow.com/a/3855394/4233593
     * @param {string} query
     */
    getQueryStringParameters(query)
    {
        return query
            ? (/^[?#]/.test(query) ? query.slice(1) : query)
                .split('&')
                .reduce((params, param) => {
                        let [key, value] = param.split('=');
                        params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
                        return params;
                    }, {}
                )
            : {}
    }
}

module.exports = Storage;
