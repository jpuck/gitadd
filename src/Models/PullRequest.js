class PullRequest
{
    constructor(source, target, status)
    {
        this.source = source;
        this.target = target;
        this.status = status || 'ok';
    }
}

module.exports = PullRequest;
