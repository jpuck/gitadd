class Branch
{
    constructor(name)
    {
        this.name = name;
        this.isBase = true;
        this.directNeighbors = [];
        this.neighborhood = [];
    }
}

module.exports = Branch;
