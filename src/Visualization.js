const WIDTH = 960;
const HEIGHT = 480;
const NODE_DIAMETER = 12;
const LINK_LENGTH = 60;

const Distributer = require('./Distributer');

class Visualization
{
    constructor(pullRequests)
    {
        this.pullRequests = pullRequests;
    }

    render(domReference)
    {
        const force = d3.layout.force()
            .nodes(d3.values(this.pullRequests.branches))
            .links(this.pullRequests)
            .size([WIDTH, HEIGHT])
            .linkDistance(LINK_LENGTH)
            .charge(-300)
            .on('tick', () => {
                path.attr('d', this.linkLine);
                circle.attr('transform', this.transform);
                text.attr('transform', this.transform);
            })
            .on('start', () => this.anchorBranchesWithoutMergeTargets())
            .start();

        d3.select(`${domReference} svg`).remove();
        const svg = d3.select(domReference)
            .append('svg')
            .attr('width', '100%')
            .attr('height', '100%')
            .attr('viewBox', `0 0 ${WIDTH} ${HEIGHT}`)
            .attr('preserveAspectRatio','xMinYMin');

        // Per-type markers, as they don't inherit styles.
        svg.append('defs').selectAll('marker')
            .data(['ok', 'behind', 'conflicting', 'failing'])
            .enter()
            .append('marker')
            .attr('id', d => d)
            .attr('viewBox', '0 -5 10 10')
            .attr('refX', 15)
            .attr('refY', 0)
            .attr('markerWidth', 6)
            .attr('markerHeight', 6)
            .attr('orient', 'auto')
            .append('path')
            .attr('d', 'M0,-5L10,0L0,5');

        const path = svg.append('g').selectAll('path')
            .data(force.links())
            .enter()
            .append('path')
            .attr('class', pr => `link ${pr.status}`)
            .attr('marker-end', pr => `url(#${pr.status})`);

        const circle = svg.append('g').selectAll('circle')
            .data(force.nodes())
            .enter()
            .append('circle')
            .attr('r', NODE_DIAMETER / 2)
            .call(force.drag);

        const text = svg.append('g').selectAll('text')
            .data(force.nodes())
            .enter()
            .append('text')
            .attr('x', 8)
            .attr('y', '.31em')
            .text(branch => branch.name);
    }

    anchorBranchesWithoutMergeTargets()
    {
        const distributer = new Distributer();
        const bases = [];

        this.pullRequests.branches.forEach(branch => {
            if (branch.isBase) {
                bases.push(branch);
            }
        });

        bases.forEach((base, index) => {
            base.fixed = true;
            base.px = distributer.getRelativePosition(WIDTH, bases.length, index);
            base.py = HEIGHT - (HEIGHT / 10);
        });
    }

    linkLine(d)
    {
        return `M ${d.source.x}, ${d.source.y} L ${d.target.x}, ${d.target.y}`;
    }

    transform(d)
    {
        return `translate(${d.x}, ${d.y})`;
    }
}

module.exports = Visualization;
