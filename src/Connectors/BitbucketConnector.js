const API_BASE_URL = 'https://api.bitbucket.org/2.0';
const axios = require('axios');

class BitbucketConnector
{
    constructor(token)
    {
        this.token = token;
    }

    async get(uri, parameters)
    {
        try {
            parameters.access_token = this.token;

            const response = await axios.get(`${API_BASE_URL}/${uri}`, {
                params: parameters,
            });

            return response.data;
        } catch (error) {
            console.error(error);
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.error(error.response.data);
            } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.error(error.request);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.error('Error', error.message);
            }
        }
    }
}

module.exports = BitbucketConnector;
