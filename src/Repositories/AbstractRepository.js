const PullRequest = require ('../Models/PullRequest');

class AbstractRepository
{
    /**
     * Sets the distinguished branch population on the property "branches"
     * Identifies branches which have no target as base branches
     *
     * @param {PullRequest[]} pullRequests Array of pull requests
     * @return {PullRequest[]}
     */
    distinguishBranches(pullRequests)
    {
        const branches = this.getDistinctBranchesIndexedByName(pullRequests);
        pullRequests.branches = Object.values(branches);
        return this.deduplicateBranchInstances(pullRequests, branches);
    }

    getDistinctBranchesIndexedByName(pullRequests)
    {
        const branches = {};

        pullRequests.forEach(pr => {
            [pr.source, pr.target].forEach(branch => {
                if (!(branch.name in branches)) {
                    branches[branch.name] = branch;
                }
            });

            // source branches cannot be a base
            branches[pr.source.name].isBase = false;
        });

        return branches;
    }

    deduplicateBranchInstances(pullRequests, branches)
    {
        pullRequests.forEach(pr => {
            pr.source = branches[pr.source.name];
            pr.target = branches[pr.target.name];
        });

        return pullRequests;
    }
}

module.exports = AbstractRepository;
