const PullRequest = require ('../Models/PullRequest');
const Branch = require ('../Models/Branch');
const AbstractRepository = require('./AbstractRepository');

class BitbucketRepository extends AbstractRepository
{
    constructor(connector)
    {
        super();
        this.connector = connector;
    }

    async getPullRequests(accountRepository)
    {
        const pullRequests = [];
        const data = await this.connector.get(`repositories/${accountRepository}/pullrequests`, {
            pagelen: 50,
        });

        data.values.forEach(pr => {
            let source = new Branch(pr.source.branch.name);
            let target = new Branch(pr.destination.branch.name);
            pullRequests.push(new PullRequest(source, target));
        });

        return this.distinguishBranches(pullRequests);
    }
}

module.exports = BitbucketRepository;
