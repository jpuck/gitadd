const AbstractRepository = require('./AbstractRepository');
const PullRequest = require ('../Models/PullRequest');
const Branch = require ('../Models/Branch');

class ObjectRepository extends AbstractRepository
{
    constructor(data)
    {
        super();
        this.data = data;
    }

    async getPullRequests()
    {
        const pullRequests = [];

        this.data.forEach(pr => {
            let source = new Branch(pr.source);
            let target = new Branch(pr.target);
            pullRequests.push(new PullRequest(source, target, pr.status));
        });

        return this.distinguishBranches(pullRequests);
    }
}

module.exports = ObjectRepository;
