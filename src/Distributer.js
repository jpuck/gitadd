class Distributer
{
    getRelativePosition(units, population, index)
    {
        const blockSize = units / population;
        const unitRegionMax = blockSize * (index + 1);
        return unitRegionMax - (blockSize / 2);
    }
}

module.exports = Distributer;
