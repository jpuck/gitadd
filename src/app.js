const VISUALIZATION_DOM_REFERENCE = '#d3-div';
const BITBUCKET_OAUTH_URL = `https://bitbucket.org/site/oauth2/authorize?client_id=${BITBUCKET_CLIENT_ID}&response_type=token`;

const ObjectRepository = require('./Repositories/ObjectRepository');
const BitbucketConnector = require('./Connectors/BitbucketConnector');
const BitbucketRepository = require('./Repositories/BitbucketRepository');
const Visualization = require('./Visualization');
const Storage = require('./Storage');

const storage = new Storage();
const token = storage.getToken();
const dataSource = token
    ? new BitbucketRepository(new BitbucketConnector(token))
    : new ObjectRepository([
        {source: 'feature/foo', target: 'develop', status: 'behind'},
        {source: 'task/model', target: 'feature/foo', status: 'conflicting'},
        {source: 'task/repo', target: 'task/model', status: 'failing'},
        {source: 'fix/bar', target: 'master', status: 'ok'},
        {source: 'feature/baz', target: 'develop', status: 'ok'},
        {source: 'task/contracts', target: 'feature/foo', status: 'ok'},
    ]);

renderPullRequests(dataSource);

document.getElementById('account/repository').value = token ? storage.get('account/repository') : '';
document.getElementById('account-repository-form').addEventListener('submit', e => {
    e.preventDefault();

    const accountRepository = document.getElementById('account/repository').value;
    storage.set('account/repository', accountRepository)

    if (!(token)) {
        window.location.replace(BITBUCKET_OAUTH_URL);
        return false;
    }

    renderPullRequests(dataSource);

    return false;
});

function renderPullRequests(repository)
{
    const accountRepository = storage.get('account/repository');
    repository.getPullRequests(accountRepository).then(pullRequests => {
        const visualization = new Visualization(pullRequests);
        visualization.render(VISUALIZATION_DOM_REFERENCE);
    });
}
