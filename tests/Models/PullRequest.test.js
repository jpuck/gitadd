const Branch = require('../../src/Models/Branch');
const PullRequest = require('../../src/Models/PullRequest');

test('get properties', () => {
    const source = new Branch('source');
    const target = new Branch('target');
    const systemUnderTest = new PullRequest(source, target);
    expect(systemUnderTest.source).toBe(source);
    expect(systemUnderTest.target).toBe(target);
    expect(systemUnderTest.status).toBe('ok');
});

test('sets status', () => {
    const source = new Branch('source');
    const target = new Branch('target');
    const expected = 'conflicting';
    const systemUnderTest = new PullRequest(source, target, expected);
    expect(systemUnderTest.status).toBe(expected);
});
