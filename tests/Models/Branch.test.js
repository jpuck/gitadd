const Branch = require('../../src/Models/Branch');

test('get name', () => {
    const expected = 'foo';
    const systemUnderTest = new Branch(expected);
    expect(systemUnderTest.name).toBe(expected);
});

test('get direct neighbor', () => {
    const systemUnderTest = new Branch('bar');
    expect(systemUnderTest.directNeighbors.length).toBe(0);

    const expected = new Branch('foo');
    systemUnderTest.directNeighbors.push(expected);
    expect(systemUnderTest.directNeighbors.includes(expected)).toBe(true);

    const unrelated = new Branch('nobody');
    expect(systemUnderTest.directNeighbors.includes(unrelated)).toBe(false);
});

test('get neighborhood', () => {
    const systemUnderTest = new Branch('bar');
    expect(systemUnderTest.neighborhood.length).toBe(0);

    const expected = new Branch('foo');
    systemUnderTest.neighborhood.push(expected);
    expect(systemUnderTest.neighborhood.includes(expected)).toBe(true);

    const unrelated = new Branch('nobody');
    expect(systemUnderTest.neighborhood.includes(unrelated)).toBe(false);
});

test('branch default is base', () => {
    const systemUnderTest = new Branch('base');
    expect(systemUnderTest.isBase).toBe(true);
})
