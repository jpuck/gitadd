const PullRequest = require('../../src/Models/PullRequest');
const Branch = require('../../src/Models/Branch');
const data = [
    {source: "feature/foo", target: "develop", status: "behind"},
    {source: "task/model", target: "feature/foo", status: "conflicting"},
    {source: "task/repo", target: "task/model", status: "failing"},
    {source: "fix/bar", target: "master", status: "ok"},
    {source: "feature/baz", target: "develop", status: "ok"},
    {source: "task/contracts", target: "feature/foo", status: "ok"},
];
const ObjectRepository = require('../../src/Repositories/ObjectRepository');
const systemUnderTest = new ObjectRepository(data);

test('get pull requests', async () => {
    const actual = await systemUnderTest.getPullRequests();
    expect(actual.length).toBe(6);

    actual.forEach(pr => {
        expect(pr).toBeInstanceOf(PullRequest);
    });

    expect(actual[0].source.name).toBe('feature/foo');
    expect(actual[0].target.name).toBe('develop');
    expect(actual[0].status).toBe('behind');

    expect(actual[1].source.name).toBe('task/model');
    expect(actual[1].target.name).toBe('feature/foo');
    expect(actual[1].status).toBe('conflicting');

    expect(actual[2].source.name).toBe('task/repo');
    expect(actual[2].target.name).toBe('task/model');
    expect(actual[2].status).toBe('failing');

    expect(actual[3].source.name).toBe('fix/bar');
    expect(actual[3].target.name).toBe('master');
    expect(actual[3].status).toBe('ok');

    expect(actual[4].source.name).toBe('feature/baz');
    expect(actual[4].target.name).toBe('develop');
    expect(actual[4].status).toBe('ok');

    expect(actual[5].source.name).toBe('task/contracts');
    expect(actual[5].target.name).toBe('feature/foo');
    expect(actual[5].status).toBe('ok');

    expect(actual[0].target).toBe(actual[4].target); // develop
    expect(actual[0].source).toBe(actual[5].target); // feature/foo
    expect(actual[1].target).toBe(actual[5].target); // feature/foo
    expect(actual[1].source).toBe(actual[2].target); // task/model
});

test('get pull request branches', async () => {
    const prs = await systemUnderTest.getPullRequests();
    const actual = prs.branches;
    expect(actual.length).toBe(8);
    actual.forEach(branch => {
        expect(branch).toBeInstanceOf(Branch);
    });

    expect(actual[0].name).toBe('feature/foo');
    expect(actual[0].isBase).toBe(false);

    expect(actual[1].name).toBe('develop');
    expect(actual[1].isBase).toBe(true);

    expect(actual[2].name).toBe('task/model');
    expect(actual[2].isBase).toBe(false);

    expect(actual[3].name).toBe('task/repo');
    expect(actual[3].isBase).toBe(false);

    expect(actual[4].name).toBe('fix/bar');
    expect(actual[4].isBase).toBe(false);

    expect(actual[5].name).toBe('master');
    expect(actual[5].isBase).toBe(true);

    expect(actual[6].name).toBe('feature/baz');
    expect(actual[6].isBase).toBe(false);

    expect(actual[7].name).toBe('task/contracts');
    expect(actual[7].isBase).toBe(false);
});
