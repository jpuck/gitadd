const PullRequest = require('../../src/Models/PullRequest');
const Branch = require('../../src/Models/Branch');
const BitbucketRepository = require('../../src/Repositories/BitbucketRepository');

test('get pull requests', async () => {
    const json = require('../fixtures/bitbucket.json');
    const bitbucketConnectorMock = {get: jest.fn().mockReturnValueOnce(json)};
    const systemUnderTest = new BitbucketRepository(bitbucketConnectorMock);

    const actual = await systemUnderTest.getPullRequests();

    expect(actual.length).toBe(3);
    actual.forEach(pr => {
        expect(pr).toBeInstanceOf(PullRequest);
    });

    expect(actual[0].source.name).toBe('task/bar');
    expect(actual[0].target.name).toBe('feature/foo');

    expect(actual[1].source.name).toBe('feature/foo');
    expect(actual[1].target.name).toBe('develop');

    expect(actual[2].source.name).toBe('fix/hyper');
    expect(actual[2].target.name).toBe('master');

    expect(actual[0].target).toBe(actual[1].source);
});

test('get pull request branches', async () => {
    const json = require('../fixtures/bitbucket.json');
    const bitbucketConnectorMock = {get: jest.fn().mockReturnValueOnce(json)};
    const systemUnderTest = new BitbucketRepository(bitbucketConnectorMock);

    const prs = await systemUnderTest.getPullRequests();
    const actual = prs.branches;

    expect(actual.length).toBe(5);
    actual.forEach(branch => {
        expect(branch).toBeInstanceOf(Branch);
    });

    expect(actual[0].name).toBe('task/bar');
    expect(actual[0].isBase).toBe(false);

    expect(actual[1].name).toBe('feature/foo');
    expect(actual[1].isBase).toBe(false);

    expect(actual[2].name).toBe('develop');
    expect(actual[2].isBase).toBe(true);

    expect(actual[3].name).toBe('fix/hyper');
    expect(actual[3].isBase).toBe(false);

    expect(actual[4].name).toBe('master');
    expect(actual[4].isBase).toBe(true);
});
