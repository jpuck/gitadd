const Distributer = require('../src/Distributer');

test('get relative position for a single object is in the center', () => {
    const units = 100;
    const population = 1;
    const index = 0;
    const expected = 50;
    const distributer = new Distributer();
    expect(distributer.getRelativePosition(units, population, index)).toBe(expected);
});

test('get relative position for first object', () => {
    const units = 100;
    const population = 2;
    const index = 0;
    const expected = 25;
    const distributer = new Distributer();
    expect(distributer.getRelativePosition(units, population, index)).toBe(expected);
});

test('get relative position for second object', () => {
    const units = 100;
    const population = 2;
    const index = 1;
    const expected = 75;
    const distributer = new Distributer();
    expect(distributer.getRelativePosition(units, population, index)).toBe(expected);
});

test('get relative position for 3 out of 5 objects', () => {
    const units = 500;
    const population = 5;
    const index = 2;
    const expected = 250;
    const distributer = new Distributer();
    expect(distributer.getRelativePosition(units, population, index)).toBe(expected);
});
