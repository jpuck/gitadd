require('dotenv').config();
const inProduction = (process.env.NODE_ENV === 'production');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const webpack = require('webpack');

module.exports = {
    mode: inProduction ? 'production' : 'development',
    entry: {
        app: [
            './src/app.js',
            './sass/app.scss',
        ],
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: '[name].[chunkhash].js',
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader", // translates CSS into CommonJS
                    "sass-loader", // compiles Sass to CSS, using Node Sass by default
                ],
            },
        ],
    },
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                cache: true,
                parallel: true,
                sourceMap: true, // set to true if you want JS source maps
            }),
            new OptimizeCSSAssetsPlugin({
                cssProcessorPluginOptions: {
                    preset: ['default', { discardComments: { removeAll: true } }],
                },
            }),
        ],
    },
    plugins: [
        new CleanWebpackPlugin(['public'], {exclude: '.gitignore'}),
        new MiniCssExtractPlugin({
            filename: "[name].[contenthash].css",
        }),
        new HtmlWebpackPlugin({
           template: './html/app.html',
           minify: {
               collapseWhitespace: inProduction,
               removeComments: inProduction,
            },
        }),
        new webpack.DefinePlugin({
            BITBUCKET_CLIENT_ID: JSON.stringify(process.env.BITBUCKET_CLIENT_ID),
        }),
    ],
};
